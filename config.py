# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive.readonly']

# "full" gets all files from the drive
# "incremental" gets modified/new files from drive since the date specified.
BACKUP_METHOD = "full"

# date to look back until for drive changes, formatted as 'yyyy-mm-dd', used if BACKUP_METHOD = "incremental"
INCREMENTAL_DATE = '2022-01-01'

# "local" to download from gdrive locally
# "s3" to download from gdrive then upload to s3 bucket (each file temporarily downloaded to buffer and wiped after upload)
STORAGE_METHOD = "s3"

# THE MODE IN WHICH TO RETRIEVE THE GDRIVE META
# "normal" gets the meta from gdrive, processes, then downloads
# "json" reads files.json & folders.json or processed.json and then downloads the files.
META_MODE = "normal"

# used if META_MODE = "read"
# path can be relative (to script) or absolute, trailing slash included
# leave path blank if files are in root of script
# path is to folder containing the .json files, do not specify filename
# files in the path specified should be named "files.json" & "folders.json" or "processed.json" respectively
JSON_LOC = "backups/2022-01-01/"

# "personal" targets the standard gdrive
# "shared" targets one or more shared drives
DRIVE_TYPE = "shared"

# The IDs and Names of the shared drives root folder to traverse and backup.
# Currently manual, can be found via web browser URI
DRIVE_ID = ["1a2b3c4d5e"]
DRIVE_NAME = ["MY SHARED DRIVE XYZ"]

# Your AWS s3 credentials. Leave blank if using aws config file
# Manully specifying keys here is not recommended and currently untested
# use the aws cli `aws configure` to set environment keys, or create the file yourself ~/.aws/credentials
AWS_KEY = ""
AWS_SECRET = ""
AWS_REGION = "us-east-1"
AWS_BUCKET = "my-aws-bucket-name"

# DEBUG LOG, change to True to enable
DEBUG = False
# Dumping of folders.json, files.json and processed.json after fetching from gdrive, change to True to enable
JSON_DUMP = False