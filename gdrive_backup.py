from __future__ import print_function
import io
import re
import json
import logging
import argparse
import os, os.path
from config import *
from datetime import datetime

# AWS API
import sys
import boto3
from botocore.exceptions import ClientError
        
# GDRIVE API
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload

# GLOBALS
TIME_START = datetime.now()
DATE = TIME_START.strftime("%Y-%m-%d")
FILE_LIST, FOLDER_LIST, FAILED_LIST = [], [], []
FILE_COUNT, FOLDER_COUNT = 0, 0

def main():
    # INIT SUMMARY
    logging.info(f'================== APP INITIALIZED ===================')
    logging.info(f'  Start Time: {TIME_START.strftime("%Y-%m-%d %H:%M:%S")}')
    logging.info(f'  Backup: {BACKUP_METHOD} |  Storage: {STORAGE_METHOD}')
    logging.info(f'  Incremental Date: {INCREMENTAL_DATE}') if BACKUP_METHOD == "incremental" else None
    logging.info(f'  GDrive Name: {DRIVE_NAME[0]} | GDrive Type: {DRIVE_TYPE}')
    logging.info(f'  GDrive ID: {DRIVE_ID[0]} | Meta Mode: {META_MODE}')
    logging.info(f'  S3 BUCKET: {AWS_BUCKET} | S3 REGION: {AWS_REGION}') if STORAGE_METHOD == "s3" else None
    logging.info(f'======================================================')
    logging.info(f'[NOTE] JSON_DUMP IS SET TO TRUE') if JSON_DUMP is True else None
    
    # Init s3 connection
    s3 = init_s3()

    try:
        # Create drive service
        CREDS = authenticate()
        GDRIVE = build('drive', 'v3', credentials=CREDS)

        # GET AVAILABLE DRIVES
        #get_drives(GDRIVE) future dev

        # GET FILE META
        if META_MODE == "json":
            meta_build = load_meta(JSON_LOC)
        else:
            meta_build = True
            get_meta(GDRIVE)

        # BUILD FILE PATHS
        if meta_build is True:
            try:
                build_paths()
            except Exception as e:
                logging.error(f'Failed to build folder hierarchy! [ERR: {e}]')

        # TRANSFER FILES
        logging.info(f'[NOTE] STARTING FILE TRANSFERS FOR [{len(FILE_LIST)}] FILES')
        i = 0
        for item in FILE_LIST:
            i += 1
            transfer_file(GDRIVE, item['name'], item['id'], item['type'], item['path'], i, s3)

    # Handle errors from drive API.
    except HttpError as e:
        logging.error(f'driveapi err: {e}')

    # EXIT SUMMARY
    logging.info(f'========= FINISHING SUMMARY ===========')
    logging.info(f'  TOTAL TIME ELAPSED: {calc_time(TIME_START)}')
    logging.info(f'  TOTAL FAILED: {len(FAILED_LIST)}')
    logging.info(f'  ITEMS FAILED: {FAILED_LIST}')
    logging.info(f'=======================================')

    # UPLOAD debug.log to S3
    if s3 is not None:
        logging.info(f'UPLOADING debug.log')
        with open(f"{DATE}/debug.log", 'rb') as file:
            s3_upload(s3, file, AWS_BUCKET, "")

# =========================
# LIST DRIVES
# =========================
def get_drives(GDRIVE):
    page_token, drives = None, {}
    logging.info('Getting DRIVE ids...')
    while True:
        response = GDRIVE.drives().list(
                fields='nextPageToken, drives(id, name)',
                useDomainAdminAccess = True,
                pageToken=page_token).execute()
        for drive in response.get('drives', []):
            drives.append(eval('{"drive":"{}","drive_id":"{}"}' % (drive.get('title'), drive.get('id'))))
            logging.info('Found shared drive without organizer: %s (%s)' % (drive.get('title'), drive.get('id')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    return drives

# =========================
# GET FILE META
# =========================
def get_meta(GDRIVE):
    global FILE_COUNT, INCREMENTAL_DATE
    folder_count, total_size, time_start = 0, 0, datetime.now()

    # Generate the arguments to pass
    kwargs = {
        "corpora" : "drive",
        "pageSize" : 1000,
        "pageToken" : None,
        "fields" : "nextPageToken, files(id, name, parents, mimeType)",
        "q" : "mimeType = 'application/vnd.google-apps.folder'",
    }
    # Shared Drive
    kwargs.update({
        "driveId" : DRIVE_ID[0],
        "supportsAllDrives" : True,
        "includeItemsFromAllDrives" : True,
    }) if DRIVE_TYPE == "shared" else None

    #========== FETCH FOLDER META
    logging.info("Fetching folder meta...")
    while True:
        response = GDRIVE.files().list(**kwargs).execute()

        if not response.get('files', []):
            logging.info('No folders found.')
            break
        
        for file in response.get('files', []):
            FOLDER_LIST.append(eval('{"name":"%s", "id":"%s", "parent_id":"%s", "type":"%s"}' % (
                        escape(file.get('name')), file.get('id'), get_parent(file), file.get('mimeType'))))
            folder_count += 1
            print('[PROCESSING..] %d folders found...' % folder_count, end='\r')  
        sys.stdout.write('\x1b[2K')

        kwargs.update({"pageToken" : response.get('nextPageToken', None)})
        if kwargs['pageToken'] is None: break

    logging.info(f"TOTAL FOLDERS FOUND: {folder_count} | TIME ELAPSED: {calc_time(time_start)}")
    dump_json("folders") if JSON_DUMP is True else None

    #========== FETCH FILE META
    kwargs.update({
        "fields" : "nextPageToken, files(id, name, parents, mimeType, size)",
        "q" : "mimeType != 'application/vnd.google-apps.folder'",
    })
    # Incremental check
    kwargs.update({
        "q" : "modifiedTime > '%sT00:00:00' and mimeType != 'application/vnd.google-apps.folder' or createdTime > '%sT00:00:00' and mimeType != 'application/vnd.google-apps.folder'" % (INCREMENTAL_DATE,INCREMENTAL_DATE)
    }) if BACKUP_METHOD == "incremental" else None
    
    time_start = datetime.now()
    logging.info("Fetching file meta...")
    while True:
        response = GDRIVE.files().list(**kwargs).execute()

        if not response.get('files', []):
            logging.info('No files found.')
            break
        
        for file in response.get('files', []):
            filesize = 0 if file.get('size') is None else int(file.get('size'))
            FILE_LIST.append(eval('{"name":"%s", "id":"%s", "parent_id":"%s", "type":"%s", "size":"%s"}' % (
                    escape(file.get('name')), file.get('id'), get_parent(file), file.get('mimeType'), filesize)))
            total_size += filesize
            FILE_COUNT += 1
            print('[PROCESSING..] %d files found...' % FILE_COUNT, end='\r')
        sys.stdout.write('\x1b[2K') #clear line
        
        kwargs.update({"pageToken" : response.get('nextPageToken', None)})
        if kwargs['pageToken'] is None: break

    logging.info(f"TOTAL FILES FOUND: {FILE_COUNT} | ~{round(total_size/1000000,2)} MB | TIME ELAPSED: {calc_time(time_start)}")
    dump_json("files") if JSON_DUMP is True else None

# =========================
# CREATE FOLDER HIERARCHY
# =========================
def build_paths():
    global FILE_COUNT
    i = 0
    temp_id = ""
    temp_path = []
    time_start = datetime.now()

    # Loop through each file, if parent_id is not the drive_id then
    # loop through each folder and append name to path
    logging.info('Generating folder hierarchy...')
    for item in FILE_LIST:
        temp_id = item["parent_id"]
        # traverse folders
        while temp_id != DRIVE_ID[0]:
            if temp_id == "None":
                break
            for folder in FOLDER_LIST:
                if folder["id"] == temp_id:
                    temp_id = folder["parent_id"]
                    temp_path.insert(0, folder["name"])
                    break
        i += 1 # count
        temp_path.insert(0, DRIVE_NAME[0])
        temp_path.append(item["name"])
        item["path"] = "/".join(temp_path)
        temp_path.clear()
        print('[PROCESSING..] %d/%d file paths processed' % (i, len(FILE_LIST)), end='\r')
    sys.stdout.write('\x1b[2K') #clear line
    logging.info('%s/%s FILES PROCESSED. TIME ELAPSED: %s' % (i, FILE_COUNT, calc_time(time_start)))
    dump_json("processed") if JSON_DUMP is True else None

# =========================
# GET FILE
# =========================
def transfer_file(GDRIVE, file_name, file_id, file_type, file_path, current_count, s3=None):
    global FILE_COUNT, FAILED_LIST

    # If workspace file, then export as OpenDocument file, else normal file
    if re.match('^application/vnd\.google-apps\..+', file_type):
        if file_type == 'application/vnd.google-apps.document':
            mime_type = 'application/vnd.oasis.opendocument.text'
            mime_ext = '.odt'
        if file_type == 'application/vnd.google-apps.spreadsheet':
            mime_type = 'application/vnd.oasis.opendocument.spreadsheet'
            mime_ext = '.ods'
        if file_type == 'application/vnd.google-apps.presentation':
            mime_type = 'application/vnd.oasis.opendocument.presentation'
            mime_ext = '.odp'
        if file_type == 'application/vnd.google-apps.drawing':
            mime_type = 'application/vnd.oasis.opendocument.graphics'
            mime_ext = '.odg'
        file_path += mime_ext
        file_name += mime_ext
        request = GDRIVE.files().export_media(fileId=file_id, mimeType=mime_type)
    else:
        request = GDRIVE.files().get_media(fileId=file_id)
    
    # Download to buffer
    buffer = io.BytesIO()
    downloader = MediaIoBaseDownload(buffer, request)
    done = False
    for i in range(1,6): #retry
        buffer.seek(0)
        try:
            while done is False:
                status, done = downloader.next_chunk()
                print("[TRANSFERRING %d%%] [%s] [%s]" % (int(status.progress() * 100), file_name, file_type), end='\r')
            sys.stdout.write('\x1b[2K') #clear line, not working for some reason

        except Exception as e:
            if i != 5:
                #logging.error("[FILE %s/%s] [ATTEMPT %d/5] [%s] (ERROR: %s)" % (current_count, FILE_COUNT, i, file_path, e))
                continue
            else:
                logging.error("[FILE %s/%s] [ATTEMPT %d/5] [DOWNLOAD BUFFER FAILED] [%s] [%s] (ERROR: %s)" % (current_count, FILE_COUNT, i, file_path, file_type, e))
                FAILED_LIST.append(file_path)
                break

        # Write out buffer to storage medium
        if STORAGE_METHOD == "s3":
            try:
                s3_upload(s3, buffer, AWS_BUCKET, file_path)
                logging.info("[FILE %s/%s] [S3 UPLOAD OK] [%s] [%s]" % (current_count, FILE_COUNT, file_path, file_type))
            except Exception as e:
                logging.error("[FILE %s/%s] [S3 UPLOAD FAILED] [%s] [%s] (ERROR: %s)" % (current_count, FILE_COUNT, file_path, file_type, e))
        else:
            try:
                with save_file(f"backups/{DATE}/{file_path}", "wb") as f:
                    f.write(buffer.getvalue())
                    f.close()
                logging.info("[FILE %s/%s] [DOWNLOADED] [%s] [%s]" % (current_count, FILE_COUNT, file_path, file_type))
            except Exception as e:
                logging.error("[FILE %s/%s] [DOWNLOAD FAILED] [%s] [%s] (ERROR: %s)" % (current_count, FILE_COUNT, file_path, file_type, e))
        break

"""
=========================
HELPER FUNCTIONS
=========================
"""
# =========================
# CALCULATE ELAPSED TIME
# =========================
def calc_time(time_start):
    time_end = datetime.now()
    time_diff = time_end - time_start
    time_elapsed = divmod(time_diff.days * 86400 + time_diff.seconds, 60)
    return f"{time_elapsed[0]}m:{time_elapsed[1]}s"

# =========================
# UPLOAD TO AWS S3 BUCKET
# =========================
def s3_upload(s3, data, bucket, file_path):

    try:
        s3.upload_fileobj(data, bucket, DATE+"/"+file_path)
    except ClientError as e:
        logging.error(e)
        return False

    return True

# =========================
# SAVE FILE META TO .json FILE
# =========================
def dump_json(list_type):
    thelist = FOLDER_LIST if list_type == "folder" else FILE_LIST
    try:
        with save_file(f"backups/{DATE}/{list_type}.json", "w") as f:
            f.write(json.dumps(thelist))
            f.close()
        logging.info(f'Contents saved to {list_type}.json')
    except Exception as e:
        logging.error(f'Failed to dump contents to {list_type}.json [ERR: {e}]')

# =========================
# LOAD json FILES
# =========================
def load_meta(path=""):
    global FILE_LIST, FOLDER_LIST, FILE_COUNT, FOLDER_COUNT
    logging.info(f'[NOTE] FILE META WILL BE READ FROM .json FILES.')
    logging.info(f'Looking for .json files...')

    if os.path.isfile(path+"processed.json") is True:
        try:
            with open(path+"processed.json") as file:
                FILE_LIST = json.load(file)
                FILE_COUNT = len(FILE_LIST)
            logging.info('==================================================')
            logging.info(f'  LOADED [{FILE_COUNT}] ITEMS FROM "processed.json"')
            logging.info('==================================================')
            return False
        except Exception as e:
            logging.error(f'Failed to load processed.json ERR: {e}')

    else:
        try:
            with open(path+"files.json") as file:
                FILE_LIST = json.load(file)
                FILE_COUNT = len(FILE_LIST)
            logging.info('Loaded files.json')

            with open(path+"folders.json") as file:
                FOLDER_LIST = json.load(file)
                FOLDER_COUNT = len(FOLDER_LIST)
            logging.info('Loaded folders.json')

            logging.info('==================================================')
            logging.info(f'LOADED [{FILE_COUNT}] ITEMS FROM "files.json"')
            logging.info(f'LOADED [{FOLDER_COUNT}] ITEMS FROM "folders.json"')
            logging.info('==================================================')
            return True
        except Exception as e:
            logging.error(f'Failed to load files/folders.json ERR: {e}')

# =========================
# GET FIRST PARENT FROM PARENTS
# =========================
def get_parent(file):
    # Removes parent_id from list
    parents = file.get('parents')
    if parents is not None:
        for the_id in parents:
            parents = the_id
            break
    return parents

# =========================
# ESCAPE FILENAMES WITH SPECIAL CHARS
# =========================
def escape(filename):
    # Temporary solution, only for quotes
    return filename.replace('"','\\"').replace("'","\\'").replace("\n"," ").replace("\r"," ")

# =========================
# This allows us to create any directories that are missing before writing file
# =========================
def save_file(path, method):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    if method == 'wb':
        return open(path, 'wb')
    elif method == 'w':
        return open(path, 'w')
    elif method == 'a':
        return open(path, 'a')

# =========================
# GDrive Credential Auth
# =========================
def authenticate():
    CREDS = None
    logging.info(f'Checking GoogleAPI credentials...')
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    try:
        if os.path.exists('token.json'):
            CREDS = Credentials.from_authorized_user_file('token.json', SCOPES)
            logging.info(f'Credentials token found.')
        
        # If there are no (valid) credentials available, let the user log in.
        if not CREDS or not CREDS.valid:
            if CREDS and CREDS.expired and CREDS.refresh_token:
                CREDS.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'client_secret.json', SCOPES)
                CREDS = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(CREDS.to_json())

        return CREDS
    except Exception as e:
        logging.error(f'Failed gdrive api authentication ERR: {e}')

# =========================
# INIT S3 CONNECTION
# =========================
def init_s3():
    
    if STORAGE_METHOD == "s3":
        logging.info(f'Establishing s3 connection...')

        if AWS_KEY == "" or AWS_SECRET == "":
            s3 = boto3.client('s3')
        else:
            s3 = boto3.client('s3', AWS_REGION, AWS_KEY, AWS_SECRET)
        
        # Check if bucket exists
        bucket = None
        for item in s3.list_buckets()['Buckets']:
            if item['Name'] == AWS_BUCKET:
                bucket = True; break
        if not bucket:
            logging.error(f'Specified s3 bucket "{AWS_BUCKET}" does not exist!'); exit()
        else:
            logging.info(f's3 connection established!')

    else:
        s3 = None

    return s3

# =========================
# Handles passing of arguments to script
# =========================
def configure_args():
    global args
    all_args = argparse.ArgumentParser()

    # Add arguments to the parser
    all_args.add_argument(
        "-b",
        required=False,
        default='full',
        help="Backup mode. Can be --incremental or --full"
    )
    all_args.add_argument(
        "-t",
        required=False,
        default='None',
        help="(if -b incremental) timestamp of last backup"
    )
    all_args.add_argument(
        "-s",
        required=False,
        default='local',
        help="(storage) local / s3"
    )
    all_args.add_argument(
        "-m",
        required=False,
        default='normal',
        help="(app mode) normal (getmeta+download) / read (getmeta & writeto files.json) / download (from files.json)"
    )
    args = vars(all_args.parse_args())
    logging.debug(f"Configured Args: {args}")

# =========================
# HANDLES LOGGER
# =========================
def configure_logger():
    os.makedirs(os.path.dirname(f'backups/{DATE}/debug.log'), exist_ok=True)
    
    LOG_LEVEL = logging.DEBUG if DEBUG else logging.INFO
    logging.basicConfig(
        level=LOG_LEVEL,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler(f'backups/{DATE}/debug.log'),
            logging.StreamHandler()
        ]
    )
    logging.debug('========== DEBUG MODE ACTIVE =============')

# ==================================================
# Initialize
# ==================================================
if __name__ == '__main__':
    configure_logger()
    configure_args()
    main()
