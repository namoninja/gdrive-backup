# GDrive Backup

Backup gdrive via api with option to upload to aws s3 bucket.

# Summary

The script is in it's early stages. Currently you are able to backup GDrive locally, with folder structure intact.
You can also choose to stream straight to s3 buckets.

The script has been tested on a 100Mbps local pc, and also on a 1Gbps VPS server with a 700GB shared gdrive folder, containing 30k+ folders and 250k+ files with a maximum single file size of ~10GB. <br>
Larger data sets & single filesizes may cause issues if there is insufficient resources on host machine.<br>

Rough Time Estimates (depends on hardward + Download/Upload speeds):

* Fetch 30k FOLDERS: ~2m
* Fetch 250k FILES: ~20m
* Build 250k PATHS: ~40m
* Gdrive -> S3: ~72hrs/1.4TB (700GB DL + 700GB UP) (5.4MB/s avg)


# Instructions
```
git clone https://gitlab.com/namoninja/gdrive-backup.git
pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
pip3 install boto3
```

Go to [Google Cloud Console](https://console.cloud.google.com)<br>
Create Project: <br>
Enable API: APIs -> Library -> Google Drive API -> Enable <br>
Set scopes 'drive.readonly' <br>
Create OAuth2 credentials <br>
Download keys and rename to client_secret.json<br>
edit/configure `config.py`<br>

```
python3 gdrive-backup.py
```

# Limitations / Known Issues

* Folder hiearachy building is resource intensive on large datasets as it has to loop through each file's meta multiple times to get the parents and build a path. Code can be improved
* Currently only able to download 1 specified SHARED DRIVE per script run
* If filenames contain special characters there might be issues. Currently only escaping single & double quotes.
* Filesize is a rough estimate and does not include workspace files. May display incorrectly

# Features
* Backup GDrive files locally
* Backup GDrive files straight to s3 through buffer
* Backups organised by Date folders
* Logs and file list is recorded for each backup.
* Incremental backup
* Download/Upload retries

# Features to implement
* Passing of arguments to script
* Backup restore functionality
* Ability to choose between my-drive or shared-drive/s
* Ability to list all drives and get ids
* Ability to indicate multiple shared drives to backup

# License

GNU GPL v3+

# Links

[Google v3 API]()<br>
[Amazon AWS API]()<br>
[boto3]()<br>